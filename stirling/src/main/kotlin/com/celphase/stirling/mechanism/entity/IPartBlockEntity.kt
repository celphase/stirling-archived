package com.celphase.stirling.mechanism.entity

import com.celphase.stirling.mechanism.MechanismPart
import net.minecraft.util.math.Direction

interface IPartBlockEntity {
    fun getPartFor(direction: Direction): MechanismPart?

    fun getConnections(): List<Direction>
}