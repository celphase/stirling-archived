package com.celphase.stirling.mechanism

import com.celphase.stirling.StirlingComponents
import com.celphase.stirling.StirlingMod
import dev.onyxstudios.cca.api.v3.component.sync.AutoSyncedComponent
import net.minecraft.nbt.CompoundTag
import net.minecraft.network.PacketByteBuf
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.world.World
import java.util.*
import kotlin.collections.ArrayDeque

class MechanismRegistryComponent(private val world: World) : IMechanismRegistryComponent,
    AutoSyncedComponent {
    private val mechanisms: MutableMap<UUID, MechanismEntry> = mutableMapOf()

    override fun clientTick() {
        for (entry in mechanisms.values) {
            entry.mechanism.tick()
        }
    }

    override fun serverTick() {
        var needsSync = false

        for (entry in mechanisms.values) {
            entry.mechanism.tick()

            if (entry.mechanism.isDirty) {
                needsSync = true
            }
        }

        if (needsSync) {
            StirlingComponents.MECHANISM_REGISTRY.sync(world)
        }
    }

    override fun applySyncPacket(buf: PacketByteBuf) {
        mechanisms.forEach { it.value.needsCull = true }

        val mechanismsAmount = buf.readInt()

        for (i in 0 until mechanismsAmount) {
            val id = buf.readUuid()
            val entry = mechanisms.getOrPut(id, { MechanismEntry(Mechanism(id, world.isClient)) })
            entry.mechanism.applySyncPacket(buf)
            entry.needsCull = false
        }

        // Cull mechanisms and parts removed/no longer relevant to us
        mechanisms.values.forEach {
            if (it.needsCull) {
                it.mechanism.parts.forEach { part -> part.onMechanismRemoved?.invoke() }
            }
        }
        mechanisms.values.removeAll { it.needsCull }
    }

    override fun writeSyncPacket(buf: PacketByteBuf, recipient: ServerPlayerEntity) {
        // Serialize the mechanisms
        val mechanismsValues = mechanisms.values
        buf.writeInt(mechanismsValues.size)

        for (entry in mechanismsValues) {
            buf.writeUuid(entry.mechanism.id)
            entry.mechanism.writeSyncPacket(buf)
        }

        // TODO: Only send visible and changed mechanisms to this player, right now the entire world's mechanisms are sent
    }

    override fun readFromNbt(tag: CompoundTag) {
        // Mechanism state is ephemeral
    }

    override fun writeToNbt(tag: CompoundTag) {
        // Mechanism state is ephemeral
    }

    override fun createMechanism(): Mechanism {
        val id = UUID.randomUUID()
        StirlingMod.LOGGER.debug("Creating mechanism $id")

        val mechanism = Mechanism(id, world.isClient)
        mechanisms[mechanism.id] = MechanismEntry(mechanism)

        return mechanism
    }

    override fun removeMechanism(id: UUID) {
        StirlingMod.LOGGER.debug("Removing mechanism $id")
        mechanisms.remove(id)
    }

    override fun detachPart(part: MechanismPart) {
        if (world.isClient) {
            throw IllegalStateException("detachPart can only be called on the server")
        }

        part.mechanism.removePart(part)

        if (part.connections.isEmpty()) {
            // If no parts connect to this mechanism anymore, it's orphaned and should just be removed
            mechanisms.remove(part.mechanism.id)

            return
        }

        // Remove the part out of the neighboring connections
        part.connections.forEach { it.connections.remove(part) }

        // Go through additional connections and check if we need to split them
        val closedSet = mutableSetOf(part.connections[0])
        outer@ for (i in 1 until part.connections.size) {
            val frontier = ArrayDeque<MechanismPart>()
            val floodFillSet = mutableSetOf<MechanismPart>()

            val checkingPart = part.connections[i]
            frontier.add(checkingPart)
            floodFillSet.add(checkingPart)

            while (frontier.isNotEmpty()) {
                val frontierPart = frontier.removeFirst()

                for (connection in frontierPart.connections) {
                    // If this connection is already in the flood fill set, it's already checked or queued
                    if (floodFillSet.contains(connection)) {
                        continue
                    }

                    floodFillSet.add(connection)
                    frontier.addLast(connection)

                    // If this connection is in the closed set, it's already been assigned a mechanism previously, and
                    // thus is valid. This means the entire flood fill is connected to a valid mechanism and does not
                    // need a new one.
                    if (closedSet.contains(connection)) {
                        closedSet.addAll(floodFillSet)
                        continue@outer
                    }
                }
            }

            // If we reached this, the flood fill is done and was not found to be connected to a valid mechanism

            // Remove the parts from the old mechanism
            part.mechanism.removeAllParts(floodFillSet)

            // A new mechanism needs to be created
            val mechanism = createMechanism()

            // Reassign the parts to the new collection
            mechanism.addAllParts(floodFillSet)
            floodFillSet.forEach { it.mechanism = mechanism }

            // Remember the parts we just assigned as valid
            closedSet.addAll(floodFillSet)
        }
    }

    override fun getMechanism(id: UUID): Mechanism? = mechanisms[id]?.mechanism

    private class MechanismEntry(
        val mechanism: Mechanism,
    ) {
        var needsCull: Boolean = false
    }
}
