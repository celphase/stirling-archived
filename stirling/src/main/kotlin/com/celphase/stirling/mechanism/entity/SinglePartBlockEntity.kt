package com.celphase.stirling.mechanism.entity

import com.celphase.stirling.StirlingComponents
import com.celphase.stirling.block.entity.IClientBlockEntityLifecycle
import com.celphase.stirling.block.entity.IServerBlockEntityLifecycle
import com.celphase.stirling.mechanism.Mechanism
import com.celphase.stirling.mechanism.MechanismPart
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.nbt.CompoundTag
import net.minecraft.util.Tickable
import net.minecraft.util.math.Direction
import java.util.*

abstract class SinglePartBlockEntity(type: BlockEntityType<*>) : BlockEntity(type), Tickable, BlockEntityClientSerializable,
    IPartBlockEntity, IServerBlockEntityLifecycle, IClientBlockEntityLifecycle {
    // Server side
    protected var part: MechanismPart? = null
        private set

    // Client side
    private var mechanismId: UUID? = null
    private var mechanism: Mechanism? = null

    val localRotation get() = (part?.mechanism?.rotation ?: mechanism?.rotation ?: 0) * localMultiplier
    val localVelocity get() = (part?.mechanism?.velocity ?: mechanism?.velocity ?: 0) * localMultiplier

    var localMultiplier = 0
        private set

    private var firstTickDone = false

    override fun onServerUnload() {
        part?.let {
            val registry = StirlingComponents.MECHANISM_REGISTRY.get(world!!)
            registry.detachPart(it)
        }
    }

    override fun onClientUnload() {
        part?.let{ it.mechanism.removePart(it) }
    }

    override fun tick() {
        if (!firstTickDone) {
            firstTick()
            firstTickDone = true
        }

        if (world!!.isClient) {
            clientTick()
        }
    }

    private fun firstTick() {
        localMultiplier = if (getLocalInverted()) -1 else 1

        if (world!!.isClient) {
            return
        }

        // Create the part for this block
        part = BlockMechanism.createPartAt(world!!, pos!!, getConnections())
        part!!.onMechanismChanged = { sync() }

        sync()
    }

    private fun clientTick() {
        if (mechanism == null && mechanismId != null) {
            val registry = StirlingComponents.MECHANISM_REGISTRY.get(world!!)
            mechanism = registry.getMechanism(mechanismId!!)
        }
    }

    override fun fromClientTag(tag: CompoundTag) {
        if (tag.contains("mi")) {
            val newId = tag.getUuid("mi")

            if (mechanismId != newId) {
                mechanism = null
                mechanismId = newId
            }
        }
    }

    override fun toClientTag(tag: CompoundTag): CompoundTag {
        part?.let { tag.putUuid("mi", it.mechanism.id) }
        return tag
    }

    override fun getPartFor(direction: Direction): MechanismPart? {
        return if (getConnections().contains(direction)) {
            part
        } else {
            null
        }
    }

    abstract fun getLocalInverted(): Boolean
}