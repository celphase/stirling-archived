package com.celphase.stirling.mechanism

/**
 * Mechanism parts are managed and created through the mechanism registry, but synchronized through block entities.
 * MechanismPart instances should only exist on the server side.
 */
class MechanismPart(
    mechanism: Mechanism,
    val connections: MutableList<MechanismPart>,
) {
    init {
        if (mechanism.isClient) {
            throw IllegalStateException("MechanismPart should only be instantiated on the server")
        }
    }

    var mechanism = mechanism
        set(value) {
            field = value
            onMechanismChanged?.let { it() }
        }

    var onMechanismChanged: (() -> Unit)? = null
    var onMechanismRemoved: (() -> Unit)? = null
}