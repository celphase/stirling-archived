package com.celphase.stirling.mechanism

import net.minecraft.network.PacketByteBuf
import java.util.*

class Mechanism(
    val id: UUID,
    val isClient: Boolean,
) {
    var rotation: Int = 0
        private set

    var velocity: Int = 0
        private set

    var isDirty = true
        private set

    val parts: MutableList<MechanismPart> = mutableListOf()

    fun tick() {
        rotation += velocity
    }

    fun createPart(): MechanismPart {
        val part = MechanismPart(this, mutableListOf())
        parts.add(part)

        return part
    }

    fun removePart(part: MechanismPart) {
        if (!parts.remove(part)) {
            throw IllegalStateException("Part was not part of mechanism")
        }
    }

    fun removeAllParts(oldParts: Collection<MechanismPart>) {
        parts.removeAll(oldParts)
    }

    fun addAllParts(newParts: Collection<MechanismPart>) {
        parts.addAll(newParts)
    }

    fun applyVelocity(velocity: Int) {
        if (this.velocity != velocity) {
            this.velocity = velocity
            isDirty = true
        }
    }

    fun mergeFrom(mechanism: Mechanism) {
        parts.addAll(mechanism.parts)
        mechanism.parts.forEach { it.mechanism = this }
        mechanism.parts.clear()
    }

    fun applySyncPacket(buf: PacketByteBuf) {
        rotation = buf.readInt()
        velocity = buf.readInt()
    }

    fun writeSyncPacket(buf: PacketByteBuf) {
        buf.writeInt(rotation)
        buf.writeInt(velocity)
        isDirty = false
    }
}
