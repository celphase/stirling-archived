package com.celphase.stirling.worldlet

import net.minecraft.util.math.Quaternion
import net.minecraft.util.math.Vec3d

class Matrix3d() {
    var m00 = 0.0
    var m01 = 0.0
    var m02 = 0.0
    var m10 = 0.0
    var m11 = 0.0
    var m12 = 0.0
    var m20 = 0.0
    var m21 = 0.0
    var m22 = 0.0

    constructor(quaternion: Quaternion) : this() {
        val f = quaternion.x.toDouble()
        val g = quaternion.y.toDouble()
        val h = quaternion.z.toDouble()
        val i = quaternion.w.toDouble()
        val j = 2.0 * f * f
        val k = 2.0 * g * g
        val l = 2.0 * h * h
        m00 = 1.0 - k - l
        m11 = 1.0 - l - j
        m22 = 1.0 - j - k
        val m = f * g
        val n = g * h
        val o = h * f
        val p = f * i
        val q = g * i
        val r = h * i
        m10 = 2.0F * (m + r)
        m01 = 2.0F * (m - r)
        m20 = 2.0F * (o - q)
        m02 = 2.0F * (o + q)
        m21 = 2.0F * (n + p)
        m12 = 2.0F * (n - p)
    }

    fun transpose(): Matrix3d {
        var d = m01
        m01 = m10
        m10 = d
        d = m02
        m02 = m20
        m20 = d
        d = m12
        m12 = m21
        m21 = d
        return this
    }

    fun add(matrix: Matrix3d): Matrix3d {
        m00 += matrix.m00
        m01 += matrix.m01
        m02 += matrix.m02
        m10 += matrix.m10
        m11 += matrix.m11
        m12 += matrix.m12
        m20 += matrix.m20
        m21 += matrix.m21
        m22 += matrix.m22
        return this
    }

    fun transform(vec: Vec3d): Vec3d {
        val x = vec.x * m00 + vec.y * m01 + vec.z * m02
        val y = vec.x * m10 + vec.y * m11 + vec.z * m12
        val z = vec.x * m20 + vec.y * m21 + vec.z * m22
        return Vec3d(x, y, z)
    }
}