package com.celphase.stirling.worldlet.entity

import com.celphase.stirling.StirlingMod
import com.celphase.stirling.mixin.client.ClientPlayNetworkHandlerAccessor
import net.minecraft.network.NetworkThreadUtils
import net.minecraft.network.Packet
import net.minecraft.network.PacketByteBuf
import net.minecraft.network.listener.ClientPlayPacketListener
import java.util.*

class WorldletSpawnS2CPacket() : Packet<ClientPlayPacketListener> {
    private var id = 0
    private var uuid = UUID(0, 0)
    private var x = 0.0
    private var y = 0.0
    private var z = 0.0
    private var yaw = 0.0f

    constructor(entity: WorldletEntity) : this() {
        id = entity.entityId
        uuid = entity.uuid
        x = entity.x
        y = entity.y
        z = entity.z
        yaw = entity.yaw
    }

    override fun read(buf: PacketByteBuf) {
        id = buf.readVarInt()
        uuid = buf.readUuid()
        x = buf.readDouble()
        y = buf.readDouble()
        z = buf.readDouble()
        yaw = buf.readFloat()
    }

    override fun write(buf: PacketByteBuf) {
        buf.writeVarInt(id)
        buf.writeUuid(uuid)
        buf.writeDouble(x)
        buf.writeDouble(y)
        buf.writeDouble(z)
        buf.writeFloat(yaw)
    }

    override fun apply(listener: ClientPlayPacketListener) {
        listener as ClientPlayNetworkHandlerAccessor
        NetworkThreadUtils.forceMainThread(this, listener, listener.getClient())

        val world = listener.getWorld()

        val entity = WorldletEntity(StirlingMod.WORLDLET_ENTITY_TYPE, world)
        entity.setPos(x, y, z)
        entity.yaw = yaw
        entity.updateTrackedPosition(x, y, z)
        entity.refreshPositionAfterTeleport(x, y, z)
        entity.entityId = id
        entity.uuid = uuid
        world.addEntity(id, entity)
    }
}
