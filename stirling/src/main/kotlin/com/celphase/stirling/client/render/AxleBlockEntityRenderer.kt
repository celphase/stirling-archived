package com.celphase.stirling.client.render

import com.celphase.stirling.block.entity.AxleBlockEntity
import com.celphase.stirling.client.StirlingClientMod
import net.minecraft.block.PillarBlock
import net.minecraft.client.MinecraftClient
import net.minecraft.client.render.RenderLayer
import net.minecraft.client.render.VertexConsumerProvider
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher
import net.minecraft.client.render.block.entity.BlockEntityRenderer
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.client.util.math.Vector3f
import net.minecraft.client.world.ClientWorld
import net.minecraft.util.Identifier
import net.minecraft.util.math.Quaternion

class AxleBlockEntityRenderer(dispatcher: BlockEntityRenderDispatcher) :
    BlockEntityRenderer<AxleBlockEntity>(dispatcher) {
    override fun render(
        entity: AxleBlockEntity,
        tickDelta: Float,
        matrices: MatrixStack,
        vertexConsumers: VertexConsumerProvider,
        light: Int,
        overlay: Int
    ) {
        val rotation = entity.localRotation.toFloat() + (entity.localVelocity.toFloat() * tickDelta)

        val world = entity.world!! as ClientWorld
        val blockState = world.getBlockState(entity.pos)

        val renderLayer = RenderLayer.getEntityCutoutNoCull(Identifier("minecraft", "textures/atlas/blocks.png"))
        val vertexConsumer = vertexConsumers.getBuffer(renderLayer)

        val modelManager = MinecraftClient.getInstance().bakedModelManager
        val axleModel = modelManager.getModel(StirlingClientMod.AXLE_MODEL)

        matrices.push()
        matrices.translate(0.5, 0.5, 0.5)
        matrices.multiply(RenderUtil.getAxisQuaternion(blockState.get(PillarBlock.AXIS)))
        matrices.multiply(Quaternion(Vector3f.POSITIVE_Z, rotation, true))
        matrices.translate(-0.5, -0.5, -0.5)

        RenderUtil.renderBakedModel(
            axleModel,
            matrices.peek(),
            vertexConsumer,
            Vector3f(1.0f, 1.0f, 1.0f),
            light,
            overlay,
        )

        if (entity.hasGear) {
            val gearModel = modelManager.getModel(StirlingClientMod.GEAR_MODEL)
            RenderUtil.renderBakedModel(
                gearModel,
                matrices.peek(),
                vertexConsumer,
                Vector3f(1.0f, 1.0f, 1.0f),
                light,
                overlay,
            )
        }

        matrices.pop()
    }
}