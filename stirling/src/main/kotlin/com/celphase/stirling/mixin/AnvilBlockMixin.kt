package com.celphase.stirling.mixin

import com.celphase.stirling.block.ItemOnAnvilBlock
import net.minecraft.block.AnvilBlock
import net.minecraft.block.BlockState
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable

@Mixin(AnvilBlock::class)
@Suppress("unused", "unused_parameter")
class AnvilBlockMixin {
    @Inject(at = [At("HEAD")], method = ["onUse"], cancellable = true)
    private fun stirlingOnUse(state: BlockState, world: World, pos: BlockPos, player: PlayerEntity, hand: Hand, hit: BlockHitResult, info: CallbackInfoReturnable<ActionResult>) {
        val stack = player.getStackInHand(hand)

        // Only execute our injection if the item held is one we're interested in
        if (!ItemOnAnvilBlock.isValidIngredient(stack.item)) {
            return
        }

        info.returnValue = ActionResult.SUCCESS

        if (!world.isClient) {
            ItemOnAnvilBlock.tryAddToAnvil(world, pos, stack)
        }
    }
}