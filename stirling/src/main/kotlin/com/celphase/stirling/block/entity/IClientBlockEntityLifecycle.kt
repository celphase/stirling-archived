package com.celphase.stirling.block.entity

interface IClientBlockEntityLifecycle {
    fun onClientLoad() {}
    fun onClientUnload() {}
}