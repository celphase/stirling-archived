package com.celphase.stirling.block.entity

import com.celphase.stirling.StirlingMod
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable
import net.minecraft.block.AnvilBlock
import net.minecraft.block.Block
import net.minecraft.block.entity.BlockEntity
import net.minecraft.nbt.CompoundTag
import net.minecraft.block.BlockState
import net.minecraft.block.Blocks
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.inventory.Inventories
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.util.collection.DefaultedList

class ItemOnAnvilBlockEntity : BlockEntity(StirlingMod.ITEM_ON_ANVIL_BLOCK_ENTITY), BlockEntityClientSerializable {
    var inventory = ItemStack(Items.IRON_INGOT, 1)
        private set

    fun setItem(item: Item) {
        inventory = ItemStack(item, 1)
        markDirty()
        sync()
    }

    fun addToStack(item: Item): Boolean {
        if (item != inventory.item) {
            return false
        }

        val limit = when (inventory.item) {
            Items.IRON_INGOT -> 3
            Items.IRON_BLOCK -> 1
            else -> 1
        }

        if (inventory.count >= limit) {
            return false
        }

        inventory.increment(1)
        markDirty()
        sync()

        return true
    }

    fun performCraft(player: PlayerEntity): Boolean {
        // Special case for repairing
        if (inventory.item == Items.IRON_BLOCK) {
            if (world!!.isClient) {
                return true
            }

            world!!.removeBlock(pos, false)
            repairAnvil()
            return true
        }

        val craftResult = when (inventory.item) {
            Items.IRON_INGOT -> when (inventory.count) {
                1 -> ItemStack(StirlingMod.IRON_ROD_ITEM, 1)
                2 -> ItemStack(StirlingMod.IRON_PLATE_ITEM, 1)
                3 -> ItemStack(StirlingMod.AXLE_ITEM, 2)
                else -> return false
            }
            else -> return false
        }

        if (world!!.isClient) {
            return true
        }

        world!!.removeBlock(pos, false)

        // Drop the resulting item
        Block.dropStack(world, pos, craftResult)
        simulateUseAnvil(player)

        return true
    }

    private fun repairAnvil() {
        val anvilPos = pos.down()

        val oldState = world!!.getBlockState(anvilPos)
        val newState = Blocks.ANVIL.defaultState.with(
            AnvilBlock.FACING,
            oldState.get(AnvilBlock.FACING)
        )
        world!!.setBlockState(anvilPos, newState)

        world!!.syncWorldEvent(1030, anvilPos, 0)
    }

    private fun simulateUseAnvil(player: PlayerEntity) {
        val anvilPos = pos.down()

        // Check if we're gonna do any damage
        if (player.random.nextFloat() >= 0.12f) {
            // We're not, just play the noise
            world!!.syncWorldEvent(1030, anvilPos, 0)
            return
        }

        val anvilState = world!!.getBlockState(anvilPos)
        val newAnvilState = AnvilBlock.getLandingState(anvilState)

        if (newAnvilState == null) {
            world!!.removeBlock(anvilPos, false)
            world!!.syncWorldEvent(1029, anvilPos, 0)
        } else {
            world!!.setBlockState(anvilPos, newAnvilState)
            world!!.syncWorldEvent(1030, anvilPos, 0)
        }
    }

    override fun toTag(tag: CompoundTag): CompoundTag {
        super.toTag(tag)
        return toClientTag(tag)
    }

    override fun fromTag(state: BlockState, tag: CompoundTag) {
        super.fromTag(state, tag)
        fromClientTag(tag)
    }

    override fun toClientTag(tag: CompoundTag): CompoundTag {
        val list = DefaultedList.ofSize(1, inventory)

        Inventories.toTag(tag, list)

        return tag
    }

    override fun fromClientTag(tag: CompoundTag) {
        if (tag.contains("Items")) {
            val list = DefaultedList.ofSize(1, ItemStack.EMPTY)
            Inventories.fromTag(tag, list)

            inventory = list[0]
        }
    }
}