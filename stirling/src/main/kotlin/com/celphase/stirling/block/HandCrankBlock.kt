package com.celphase.stirling.block

import com.celphase.stirling.block.entity.HandCrankBlockEntity
import net.minecraft.block.*
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemPlacementContext
import net.minecraft.state.StateManager
import net.minecraft.state.property.DirectionProperty
import net.minecraft.state.property.Properties
import net.minecraft.util.ActionResult
import net.minecraft.util.BlockMirror
import net.minecraft.util.BlockRotation
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.util.shape.VoxelShape
import net.minecraft.util.shape.VoxelShapes
import net.minecraft.world.BlockView
import net.minecraft.world.World

class HandCrankBlock(settings: Settings) : Block(settings), BlockEntityProvider {
    override fun getOutlineShape(
        state: BlockState,
        world: BlockView,
        pos: BlockPos,
        context: ShapeContext,
    ): VoxelShape =
        when (state.get(FACING)) {
            Direction.NORTH -> NORTH_SHAPE
            Direction.SOUTH -> SOUTH_SHAPE
            Direction.WEST -> WEST_SHAPE
            Direction.EAST -> EAST_SHAPE
            Direction.UP -> UP_SHAPE
            Direction.DOWN -> DOWN_SHAPE
            else -> VoxelShapes.fullCube()
        }

    override fun createBlockEntity(world: BlockView?): BlockEntity =
        HandCrankBlockEntity()

    override fun getRenderType(state: BlockState?): BlockRenderType =
        BlockRenderType.ENTITYBLOCK_ANIMATED

    override fun appendProperties(builder: StateManager.Builder<Block, BlockState>) {
        builder.add(FACING)
    }

    override fun rotate(state: BlockState, rotation: BlockRotation): BlockState =
        state.with(FACING, rotation.rotate(state.get(FACING)))

    override fun mirror(state: BlockState, mirror: BlockMirror): BlockState =
        state.rotate(mirror.getRotation(state.get(FACING)))

    override fun getPlacementState(ctx: ItemPlacementContext): BlockState =
        defaultState.with(FACING, ctx.side)

    override fun onUse(
        state: BlockState,
        world: World,
        pos: BlockPos,
        player: PlayerEntity,
        hand: Hand,
        hit: BlockHitResult,
    ): ActionResult {
        if (world.isClient) {
            return ActionResult.SUCCESS
        }

        val multiplier = when (player.isInSneakingPose) {
            true -> -1
            false -> 1
        }

        val entity = world.getBlockEntity(pos) as HandCrankBlockEntity
        entity.onCrank(multiplier)

        return ActionResult.SUCCESS
    }

    companion object {
        val FACING: DirectionProperty = Properties.FACING

        val NORTH_SHAPE: VoxelShape = VoxelShapes.cuboid(0.25, 0.25, 0.75, 0.75, 0.75, 1.0)
        val SOUTH_SHAPE: VoxelShape = VoxelShapes.cuboid(0.25, 0.25, 0.0, 0.75, 0.75, 0.25)
        val WEST_SHAPE: VoxelShape = VoxelShapes.cuboid(0.75, 0.25, 0.25, 1.0, 0.75, 0.75)
        val EAST_SHAPE: VoxelShape = VoxelShapes.cuboid(0.0, 0.25, 0.25, 0.25, 0.75, 0.75)
        val UP_SHAPE: VoxelShape = VoxelShapes.cuboid(0.25, 0.0, 0.25, 0.75, 0.25, 0.75)
        val DOWN_SHAPE: VoxelShape = VoxelShapes.cuboid(0.25, 0.75, 0.25, 0.75, 1.0, 0.75)
    }
}