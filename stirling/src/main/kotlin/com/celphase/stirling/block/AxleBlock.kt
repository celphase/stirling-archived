package com.celphase.stirling.block

import com.celphase.stirling.StirlingMod
import com.celphase.stirling.block.entity.AxleBlockEntity
import net.minecraft.block.*
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.util.shape.VoxelShape
import net.minecraft.util.shape.VoxelShapes
import net.minecraft.world.BlockView
import net.minecraft.world.World

class AxleBlock(settings: Settings) : PillarBlock(settings), BlockEntityProvider {
    override fun getOutlineShape(
        state: BlockState,
        world: BlockView,
        pos: BlockPos,
        context: ShapeContext,
    ): VoxelShape =
        when (state.get(AXIS)) {
            Direction.Axis.X -> X_SHAPE
            Direction.Axis.Y -> Y_SHAPE
            Direction.Axis.Z -> Z_SHAPE
            else -> VoxelShapes.fullCube()
        }

    override fun createBlockEntity(world: BlockView?): BlockEntity =
        AxleBlockEntity()

    override fun getRenderType(state: BlockState?): BlockRenderType =
        BlockRenderType.ENTITYBLOCK_ANIMATED

    override fun onUse(
        state: BlockState,
        world: World,
        pos: BlockPos,
        player: PlayerEntity,
        hand: Hand,
        hit: BlockHitResult
    ): ActionResult {
        val stack = player.getStackInHand(hand)

        if (stack.item != StirlingMod.GEAR_ITEM) {
            return ActionResult.PASS
        }

        val entity = world.getBlockEntity(pos) as AxleBlockEntity

        if (entity.hasGear) {
            return ActionResult.FAIL
        }

        if (world.isClient) {
            return ActionResult.SUCCESS
        }

        entity.addGear()
        stack.decrement(1)

        return ActionResult.SUCCESS
    }

    override fun afterBreak(
        world: World,
        player: PlayerEntity,
        pos: BlockPos,
        state: BlockState,
        blockEntity: BlockEntity?,
        stack: ItemStack?,
    ) {
        // Drop the gear
        if ((blockEntity!! as AxleBlockEntity).hasGear) {
            dropStack(world, pos, ItemStack(StirlingMod.GEAR_ITEM, 1))
        }

        super.afterBreak(world, player, pos, state, blockEntity, stack)
    }

    companion object {
        val X_SHAPE: VoxelShape = VoxelShapes.cuboid(0.0, 0.375, 0.375, 1.0, 0.625, 0.625)
        val Y_SHAPE: VoxelShape = VoxelShapes.cuboid(0.375, 0.0, 0.375, 0.625, 1.0, 0.625)
        val Z_SHAPE: VoxelShape = VoxelShapes.cuboid(0.375, 0.375, 0.0, 0.625, 0.625, 1.0)
    }
}