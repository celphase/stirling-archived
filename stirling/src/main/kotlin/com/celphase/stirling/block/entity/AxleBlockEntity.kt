package com.celphase.stirling.block.entity

import com.celphase.stirling.StirlingMod
import com.celphase.stirling.mechanism.entity.BlockMechanism
import com.celphase.stirling.mechanism.entity.SinglePartBlockEntity
import net.minecraft.block.BlockState
import net.minecraft.block.PillarBlock
import net.minecraft.nbt.CompoundTag
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.util.math.Direction

class AxleBlockEntity : SinglePartBlockEntity(StirlingMod.AXLE_BLOCK_ENTITY) {
    var hasGear: Boolean = false
        private set

    override fun getConnections(): List<Direction> {
        val axis = world!!.getBlockState(pos).get(PillarBlock.AXIS)

        val connections = mutableListOf(
            Direction.from(axis, Direction.AxisDirection.POSITIVE),
            Direction.from(axis, Direction.AxisDirection.NEGATIVE),
        )

        // The gear adds the opposite axes
        if (hasGear) {
            if (axis != Direction.Axis.X) {
                connections.add(Direction.WEST)
                connections.add(Direction.EAST)
            }
            if (axis != Direction.Axis.Y) {
                connections.add(Direction.DOWN)
                connections.add(Direction.UP)
            }
            if (axis != Direction.Axis.Z) {
                connections.add(Direction.NORTH)
                connections.add(Direction.SOUTH)
            }
        }

        return connections
    }

    override fun getLocalInverted(): Boolean {
        return false
    }

    override fun fromTag(state: BlockState, tag: CompoundTag) {
        super.fromTag(state, tag)
        hasGear = tag.getBoolean("hg")
    }

    override fun toTag(tag: CompoundTag): CompoundTag {
        super.toTag(tag)
        tag.putBoolean("hg", hasGear)
        return tag
    }

    override fun fromClientTag(tag: CompoundTag) {
        super.fromClientTag(tag)
        hasGear = tag.getBoolean("hg")
    }

    override fun toClientTag(tag: CompoundTag): CompoundTag {
        super.toClientTag(tag)
        tag.putBoolean("hg", hasGear)
        return tag
    }

    fun addGear() {
        hasGear = true
        world!!.playSound(null, pos, SoundEvents.BLOCK_METAL_PLACE, SoundCategory.BLOCKS, 1.0f, 1.0f)
        sync()

        // Re-scan connections
        BlockMechanism.reconnectPartAt(part!!, world!!, pos, getConnections())
    }
}