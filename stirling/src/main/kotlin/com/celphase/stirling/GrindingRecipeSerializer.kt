package com.celphase.stirling

import com.google.gson.JsonObject
import net.minecraft.item.ItemStack
import net.minecraft.network.PacketByteBuf
import net.minecraft.recipe.Ingredient
import net.minecraft.recipe.RecipeSerializer
import net.minecraft.util.Identifier
import net.minecraft.util.JsonHelper
import net.minecraft.util.registry.Registry

class GrindingRecipeSerializer : RecipeSerializer<GrindingRecipe> {
    override fun read(id: Identifier, json: JsonObject): GrindingRecipe {
        val ingredientJson = JsonHelper.getObject(json, "ingredient")
        val input = Ingredient.fromJson(ingredientJson)

        val outputId = JsonHelper.getString(json, "result")
        val item = Registry.ITEM.getOrEmpty(Identifier(outputId))
            .orElseThrow { IllegalStateException("Item: $outputId does not exist") }
        val output = ItemStack(item)

        return GrindingRecipe(id, input, output)
    }

    override fun read(id: Identifier, buf: PacketByteBuf): GrindingRecipe {
        val input = Ingredient.fromPacket(buf)
        val output = buf.readItemStack()
        return GrindingRecipe(id, input, output)
    }

    override fun write(buf: PacketByteBuf, recipe: GrindingRecipe) {
        recipe.input.write(buf)
        buf.writeItemStack(recipe.output)
    }
}